  $('.panel-crear-colaborador').fadeIn();

  $.get(
    "Controllers/AjaxController.php?funcion=colaboradores",            
    function(data,status){             
      if(data != 0){    
        $('.dataTables_empty').remove();
        var obj = $.parseJSON(data);                    
        for (var i in obj) {            
          var colaborador = '<tr><td>'+obj[i].nombre+'</td> <td>'+obj[i].email+'</td> <td>'+obj[i].clave+'</td> <td>'+obj[i].tipoIdentificacion+'</td> <td>'+obj[i].identificacion+'</td> <td>'+obj[i].tipo+'</td> <td><span>'+obj[i].unidadAcademica.nombre+'</span><span hidden>'+obj[i].unidadAcademica.id+'</span></td> <td>'+obj[i].permiso+'</td> <td>  <a class="editar-colaborador"><span class="label label-success"><i class="fa fa-edit"></i></span></a> <a class="eliminar-colaborador"><span class="label label-danger"><i class="fa fa-trash"></i></span></a> <span id="'+obj[i].id+'" hidden>'+obj[i].id+'</span> </td></tr>';
          $('.table-colaboradores').append(colaborador);                           
        }

        $('.dataTables-example').DataTable({
          pageLength: 25,
          responsive: true,
          dom: '<"html5buttons"B>lTfgitp',
          buttons: [
              { extend: 'copy'},
              {extend: 'csv'},
              {extend: 'excel', title: 'Colaboradores'},
              {extend: 'pdf', title: 'Colaboradores'},

              {extend: 'print',
               customize: function (win){
                      $(win.document.body).addClass('white-bg');
                      $(win.document.body).css('font-size', '10px');

                      $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
              }
              }
          ]
        });
      }           
    }
  );


  $.get(
    "Controllers/AjaxController.php?funcion=unidadesAcademicas",
    function(data,status){
      if(data != 0){
        var obj = $.parseJSON(data);                          
        for (var i in obj) {            
          var unidad = '<option value="'+obj[i]['id']+'"> '+obj[i]['nombre']+'</option>';
          $('#unidad').append(unidad);                           
          $('#nUnidad').append(unidad);                           
        }
      }
    }
  );  

  function inputTextValidation(validacionTab){
    var val = true;    
    $('.text-danger').remove();
    $(validacionTab).each(function(){
      var valor = $(this).val();
      if(valor == ''){      
        $(this).after('<p class="text-danger">Este campo es obligatorio</p>');
        val = false;
      }      
    });
    return val;
  }

  $('.input-text').change(function(){
    $(this).next().fadeOut(function(){
      $(this).remove();
    });
  });

  $('#crear').click(function(){
    if(inputTextValidation('.input-text')){
      var nombre = $('#nombre').val();
      var email = $('#email').val();
      var clave = $('#clave').val();
      var tipo = $('#tipo').val();
      var unidad = $('#unidad').val();
      var identificacion = $('#identificacion').val();
      var tipoIdentificacion = $('#tipoIdentificacion').val();
      var usuario = $('#usuario').val();
      var obj = '{"nombre":"'+nombre+'", "email":"'+email+'", "clave":"'+clave+'", "tipo":"'+tipo+'", "unidad":"'+unidad+'", "identificacion":"'+identificacion+'", "tipoIdentificacion":"'+tipoIdentificacion+'" , "usuario":"'+usuario+'"}';
      
      $.post(
        "Controllers/AjaxController.php?funcion=crearColaborador",            
        {
            colaborador : obj                
        },
        function(data,status){         
        
          if(data != 0){
            swal({                
              title: "Colaborador Agregado",
              text: "El colaborador ha sido agregado exitosamente, los detalles se mostraran a continuacion ",
              type: "success"
            }, function(){
              window.location.href = 'colaboradores.php';  
            });       
          }else{
            swal({                
              title: "Error",
              text: "Ha ocurrido un error al agregar al colaborador, Intentelo de nuevo",
              type: "error"
            }); 
          }
        }           
      );
    }
  });

  $(".table-colaboradores").on("click", ".eliminar-colaborador", function(){     
      var idColaborador = $(this).next().text();      
      
      swal({
          title: "Esta seguro?",
          text: "El colaborador se eliminara",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Si, Eliminar",
          closeOnConfirm: false
      }, function () {        
          $.post(
            "Controllers/AjaxController.php?funcion=eliminarColaborador",            
            {
                id : idColaborador
            },
            function(data,status){                              
              if(data == 1){                                
                swal({                
                  title: "Colaborador Eliminado",
                  text: "El colaborador ha sido eliminado exitosamente",
                  type: "success"
                }, function(){
                  window.location.href = 'colaboradores.php';  
                });              
              }else{
                swal("Error", "Ha habido un error al eliminar el colaborador, intentelo de nuevo", "error");          
              }                                      
          });      
      });
  });

$(".table-colaboradores").on("click", ".editar-colaborador", function(){            
    $('.panel-crear-colaborador').fadeOut(function(){
      $('.panel-editar-colaborador').fadeIn();
    });

    var idColaborador = $(this).next().next().text();    
    $('#id-colaborador').val(idColaborador);
    var nNombre = $(this).parent().parent().children().eq(0).text(); 
    var nEmail = $(this).parent().parent().children().eq(1).text(); 
    var nClave = $(this).parent().parent().children().eq(2).text(); 
    var nTipoIdentificacion = $(this).parent().parent().children().eq(3).text(); 
    var nIndetificacion = $(this).parent().parent().children().eq(4).text(); 
    var nTipo = $(this).parent().parent().children().eq(5).text(); 
    var nUnidadAcademica = $(this).parent().parent().children().eq(6).children().first().text(); 
    var nIdUnidadAcademica = $(this).parent().parent().children().eq(6).children().next().text(); 
    var nPermiso = $(this).parent().parent().children().eq(7).text(); 
    
    $('#nNombre').val(nNombre);
    $('#nEmail').val(nEmail);
    $('#nClave').val(nClave);
    $('#nTipoIdentificacion').val(nTipoIdentificacion);
    $('#nIdentificacion').val(nIndetificacion);
    $('#nTipo').val(nTipo);
    $('#nUnidadAcademica').val(nUnidadAcademica);
    $('#nUsuario').val(nPermiso);

    $("html, body").animate({ scrollTop: "1000px" });
  });

$('#editar').click(function(){  
    if(inputTextValidation('.input-text-edit')){
      var idColaborador = $('#id-colaborador').val();    
      var nombre = $('#nNombre').val();
      var email = $('#nEmail').val();
      var clave = $('#nClave').val();
      var tipo = $('#nTipo').val();
      var unidad = $('#nUnidad').val();
      var identificacion = $('#nIdentificacion').val();
      var tipoIdentificacion = $('#nTipoIdentificacion').val();
      var usuario = $('#nUsuario').val();
      var obj = '{"id":"'+idColaborador+'", "nombre":"'+nombre+'", "email":"'+email+'", "clave":"'+clave+'", "tipo":"'+tipo+'", "unidad":"'+unidad+'", "identificacion":"'+identificacion+'", "tipoIdentificacion":"'+tipoIdentificacion+'" , "usuario":"'+usuario+'"}';      
      $.post(
        "Controllers/AjaxController.php?funcion=editarColaborador",            
        {
            colaborador : obj                
        },
        function(data,status){         
          console.log(data);
          if(data != 0){
            swal({                
              title: "Colaborador Editado",
              text: "El colaborador ha sido editado exitosamente",
              type: "success"
            }, function(){
              window.location.href = 'colaboradores.php';  
            });       
          }else{
            swal({                
              title: "Error",
              text: "Ha ocurrido un error al editar al colaborador, Intentelo de nuevo",
              type: "error"
            }); 
          }
        }           
      );
    }
});

$('#cancelar').click(function(){
  $('.panel-editar-colaborador').fadeOut(function(){
    $('.panel-crear-colaborador').fadeIn();
  });
});


 

