$(document).ready(function(){
  function inputTextValidation(validacionTab){
    var val = true;    
    $('.text-danger').remove();
    $(validacionTab).each(function(){
      var valor = $(this).val();
      if(valor == ''){      
        $(this).after('<p class="text-danger">Este campo es obligatorio</p>');
        val = false;
      }      
    });
    return val;
  }

  function tableEmpty(element, table){
    $('.alert-dismissible').remove();    
    var count = 0;
    $(element).each(function(){      
      count++;
    });
    if(count == 0){      
      $(table).after('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> Debe agregar al menos un elemento para la actividad </div>');
      return false;
    }else{
      return true;      
    }      
  }

  function dateTimeFormat(date, time){
  
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var hour = time.substring(0, 2);
     var minute = time.substring(3, 5);

     var dateTime = age+'-'+month+'-'+day+' '+hour+'-'+minute;
    return dateTime;     
  }

  function dateFormat(date){
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var dateFormat = age+'-'+month+'-'+day;
    return dateFormat;     
  }

  function countElementsTable(table){
    var count = 0;
    $(table).each(function(){      
      count++;
    });
    return count;     
  }

  $('.validacion-tab1').change(function() {
    $(this).next().fadeOut(function(){
      $(this).next().remove();  
    });    
  });

  function responsSelected(){
    $('.resp-danger').remove();
    var resp = $('#responsable').text()
    if(resp == ''){
      $('#responsable').parent().after('<p class="text-danger resp-danger"> Debe indicar un responsable para la actividad</p>');
      return true;
    }else{
      return false;
    }
  }
  
  $('#add-sesion').click(function() {
    var valid = true;
    var nombre = $('#nombre-sesion').val();
    var descripcion = $('#descripcion-sesion').val();

    if(nombre != '' && descripcion != ''){
      $('.alert-dismissible').remove();        
      var fecha = $('#single_cal4').val();
      var hora = $('#hora-sesion').val();
      var num = countElementsTable('.sesion');
      var parid;
      if((num % 2) == 0){
        parid = 'odd';
      }else{
        parid = 'even';
      }
      var sesion = '<tr class="'+parid+' pointer sesion"> <td>'+(num+1)+'</td> <td>'+nombre+'</td><td>'+descripcion+'</td> <td>'+fecha+'</td> <td>'+hora+'</td> <td> <i class="fa fa-trash-o close-sesion"></i></td> </tr>';    
      $('#table-sesiones').children().next().append(sesion);
      $('#nombre-sesion').next().remove();
      $('#descripcion-sesion').next().remove();
      $('#nombre-sesion').val('');
      $('#descripcion-sesion').val('');

    }else if(nombre == ''){
      $('#nombre-sesion').after('<p class="text-danger ">Este campo es obligatorio</p>');      
    }else if(descripcion == ''){
      $('#descripcion-sesion').after('<p class="text-danger ">Este campo es obligatorio</p>');
    }
  });

  $('#nombre-sesion').change(function(){
    $(this).next().remove();
  });

  $('#descripcion-sesion').change(function(){
    $(this).next().remove();
  });

  $(".list-sesiones").on("click", ".close-sesion", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.sesion');    
  });

  $('#add-colab').click(function(){
    $('.alert-dismissible').remove();
      
      var tipo = $("#tipo-colaborador option:selected").text();  
      var unidad = $('#unidadAcademicaCol option:selected').text();
      var colab = $('#colaborador option:selected').text();
      var idColab = $('#colaborador').val();

      var num = countElementsTable('.colab');
      var parid;
      if((num % 2) == 0){
        parid = 'odd';
      }else{
        parid = 'even';
      }
      var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(num+1)+'</td> <td>'+colab+'</td> <td>'+tipo+'</td> <td>'+unidad+'</td> <td class="none">'+idColab+'</td> <td></i> <i class="fa fa-trash-o close-colab"></i></td> <input type="hidden" value="'+idColab+'" class="idColab"> </tr>';
      $('#table-colab').children().next().append(colaborador);
  });

  $(".list-colab").on("click", ".close-colab", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.colab');    
  });

  function reasignNumberItems(item){
    var count = 1;
    $(item).each(function() {
      $(this).children().first().text(count);
      count++;
    });
  }

  $('#select-resp').click(function() {
    $('.resp-danger').remove();    
    var resp = $('#responsables option:selected').text();
    var idResp = $('#responsables option:selected').val();
    
    $('#responsable').text(resp);
    $('#idResponsable').val(idResp);
  });
  
  function tipoConvocatoria(){
    var convocatoria = $("input[name='convocatoria']:checked").val();
    if(convocatoria == 'Especifica'){
      $('#tab-especifica').css('display','block');
      $('#tab-masiva').css('display','none');      
    }
    if(convocatoria == 'Masiva'){
      $('#tab-masiva').css('display','block');
      $('#tab-especifica').css('display','none');
    }    
    return convocatoria;
  }


  $('#atr-tabHome').click(function() {
    $('#home-tab').click();
  });


  $('#add-benef').click(function(){
    $('.alert-dismissible').remove();
    $('.danger-benef').remove();
    var valid = true;
    var nombre;
    var documento;
    var tipoDocumento = $('#tipo-documento').val();    
    if( $('#nombre-beneficiario').val() == '' ){
      $('#nombre-beneficiario').after('<p class="text-danger danger-benef">Campo es obligatorio</p>');    
      valid = false;
    }else{
      nombre = $('#nombre-beneficiario').val();
    }
    if( $('#documento').val() == '' ){
      $('#documento').after('<p class="text-danger danger-benef">Campo es obligatorio</p>');
      valid = false;    
    }else{
      documento = $('#documento').val();
    }
    if(valid){  
      var num = countElementsTable('.benef');
      var parid;
      if((num % 2) == 0){
        parid = 'odd';
      }else{
        parid = 'even';
      }
      var colaborador = '<tr class="'+parid+' pointer benef"> <td>'+(num+1)+'</td> <td>'+nombre+'</td> <td>'+tipoDocumento+'</td> <td>'+documento+'</td> <td><i class="fa fa-trash-o close-benef"></i></td></tr>';
      $('#table-benef').children().next().append(colaborador);
      
      $('#nombre-beneficiario').val('');
      $('#documento').val('');
    }
  });

  $(".list-benef").on("click", ".close-benef", function(){            
      $(this).parent().parent().remove();  
      reasignNumberItems('.benef');    
  });

  $('#nombre-beneficiario').change(function() {
    $(this).next().remove();
  });

  $('#documento').change(function() {
    $(this).next().remove();
  });

  $('#atr-profileTab').click(function() {
    $('#profile-tab').click();
  });




  $('#sig-tab1').click(function(){          
    var validation = true;
    if( !inputTextValidation('.validacion-tab1') ){      
      validation = false;
    }    
    if(!tableEmpty('.sesion', '#table-sesiones')){    
      validation = false;
    }
    if(validation){      
      $('#tab2').css('display','block');
      
      $('#profile-tab').click();  
    }else{
      $('#tab2').css('display','none');
      $('#tab3').css('display','none');
    }    
  });


  $('#sig-tab2').click(function() {
    var validation = true;
    if(!tableEmpty('.colab', '#table-colab')){    
      validation = false;
    }
    if(responsSelected()){
      validation = false;
    }
    if(validation){
      $('#profile-tab2').click();      
      $('#tab3').css('display','block');
    }else{
      $('#tab3').css('display','none');
    }
  });  

  $('#sig-tab3').click(function(){    
    if( tipoConvocatoria() == 'Especifica'){    
      if(tableEmpty('.benef', '#table-benef')){         
        crearActividad();  
      }
    }else if(  tipoConvocatoria() == 'Masiva'){          
      crearActividad();
    } 
  });

  function crearActividad(){

    var nombre = $('#nombre').val();
    var objetivo = $('#objetivo').val();
    var tipo = $('#tipo').val();
    var convocatoria = $("input[name='convocatoria']:checked").val();
    var listSesiones = sesiones();
    var listColab = colaboradores();            
    var responsable = $('#idResponsable').val();
    var estado = $("input[name='estado']:checked").val();
    var resultado = $('#resultado').val();      
    var fecha = dateFormat($('#single_cal1').val());      
    var temporalidad = $('#temporalidad').val();      
    var listBenef;
    var volumen;
    if( tipoConvocatoria() == 'Especifica'){
      listBenef = beneficiarios();  
      volumen = listBenef.length;
    }else if(  tipoConvocatoria() == 'Masiva'){          
      volumen = $('#volumen').val();
    }   
    var obj = '{"nombre":"'+nombre+'", "objetivo":"'+objetivo+'", "fecha":"'+fecha+'", "temporalidad":"'+temporalidad+'", "tipo":"'+tipo+'", "convocatoria":"'+convocatoria+'", "idResponsable":"'+responsable+'", "estado":"'+estado+'", "volumen":"'+volumen+'", "resultado":"'+resultado+'", "creador":"'+idUsuario+'", "sesiones": [';        
    for (var i = 0; i < listSesiones.length; i++) {
      if((i+1) == listSesiones.length){
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "fechaHora":"'+listSesiones[i].fechaHora+'"}';  
      }else{          
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "fechaHora":"'+listSesiones[i].fechaHora+'"},';  
      }      
    }

    if( tipoConvocatoria() == 'Especifica'){
      obj += '], "beneficiarios": [';
      for (var i = 0; i < listBenef.length; i++) {
        if((i+1) == listBenef.length){
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'"}'  
        }else{
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'"},'  
        }      
      }
    }

   
    obj += '], "idColaboradores": [';
    for (var i = 0; i < listColab.length; i++) {
      if((i+1) == listColab.length){
        obj += '"'+listColab[i].id+'"';  
      }else{
        obj += '"'+listColab[i].id+'",';
      }      
    }
    obj += ']}';  
    
    $.post(
      "Controllers/AjaxController.php?funcion=crearActividad",            
      {
          json : obj                
      },
      function(data,status){ 
        console.log(data);
        if(data != 0){
          swal({                
            title: "Actividad Creada",
            text: "La actividad ha sido creada exitosamente, los detalles se mostraran a continuacion ",
            type: "success"
          }, function(){
            window.location.href = 'actividad.php?idActividad='+data;                      
          }); 
        }else{
          swal({                
            title: "Error",
            text: "Ha ocurrido un error al crear la actividad",
            type: "error"
          }); 
        }
      }
    );  
  }

  $('#prueba').click(function(){
    swal({                
      title: "Error",
      text: "Ha ocurrido un error al crear la actividad",
      type: "error"
    }); 
  });

  $('#profile-tab2').click(function() {
    tipoConvocatoria();
  });

  var now = new Date();
  var month = (now.getMonth() + 1);               
  var day = now.getDate();
  if(month < 10) 
      month = "0" + month;
  if(day < 10) 
      day = "0" + day;
  var today =  month + '/' + day + '/' + now.getFullYear();
  $('#single_cal1').val(today);    
  $('#single_cal4').val(today);

  function sesiones(){    
    var list = new Array();
    var sesion;
    $('.sesion').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "descripcion":$(this).children().eq(2).text(), 
          "fechaHora": dateTimeFormat( $(this).children().eq(3).text() ,  $(this).children().eq(4).text() ),          
        }
        list.push(sesion);                    
    }); 
    return list;
  }

  function colaboradores(){    
    var list = new Array();
    var colab;
    $('.idColab').each(function() {      
        colab = { 
          "id":$(this).val()                  
        }
        list.push(colab);                    
    }); 
    return list;
  }

  function beneficiarios(){    
    var list = new Array();
    var benef;
    $('.benef').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "tipoIdentificacion":$(this).children().eq(2).text(),
          "identificacion":$(this).children().eq(3).text()
        }
        list.push(sesion);                    
    }); 
    return list;
  }
  


// ======================================

  $.get("Controllers/AjaxController.php?funcion=tiposColaborador", 
    function(data, status){            
      if(data != 0){
        var obj = JSON.parse(data);
        var option = '';
        for (i in obj){
          option += '<option>'+obj[i]+'</option>';        
        }
        $('#tipo-colaborador').append(option);    
        $('#tipo-responsable').append(option);                  
      }
    }
  );

  $('#tipo-colaborador').change(function(){    
    $('#unidadAcademicaCol').empty();
    $('#colaborador').empty();
    var tipo = $(this).val()
    $.post(
      "Controllers/AjaxController.php?funcion=areasAcademicasTipo",            
      {
          tipo : tipo
      },
      function(data,status){ 
        if(data != 0){
          var obj = JSON.parse(data);        
          var option = '';
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['area']+'</option>';        
          }        
          $('#unidadAcademicaCol').empty().append(option);
        }                                   
      }
    );
  });

  $('#unidadAcademicaCol').change(function(){
    var tipo = $('#tipo-colaborador').val();
    var idArea = $(this).val();    
    $.post(
      "Controllers/AjaxController.php?funcion=colaboradoresArea",            
      {
          tipo: tipo, 
          idArea : idArea
      },
      function(data,status){         
        if(data != 0){
          var obj = JSON.parse(data);
          var option = '';
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['nombre']+'</option>';        
          }        
          $('#colaborador').empty().append(option);
        }                           
      }
    );
  }); 


  //============================================== 


  $('#tipo-responsable').change(function(){    
    $('#unidadAcademica').empty();
    $('#responsables').empty();
    var tipo = $(this).val();      

    $.post(
      "Controllers/AjaxController.php?funcion=areasAcademicasTipo",            
      {
          tipo : tipo
      },
      function(data,status){ 
        if(data != 0){
          var obj = JSON.parse(data);        
          var option = '';
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['area']+'</option>';        
          }        
          $('#unidadAcademica').empty().append(option);
        }                                   
      }
    );
  });

  $('#unidadAcademica').change(function(){
    var tipo = $('#tipo-responsable').val();
    var idArea = $(this).val();        
    $.post(
      "Controllers/AjaxController.php?funcion=colaboradoresArea",            
      {
          tipo : tipo,
          idArea : idArea
      },
      function(data,status){         
        if(data != 0){
          var obj = JSON.parse(data);
          var option = '';
          for (i in obj){
            option += '<option value="'+obj[i]['id']+'">'+obj[i]['nombre']+'</option>';        
          }        
          $('#responsables').empty().append(option);
        }                           
      }
    );
  }); 


});