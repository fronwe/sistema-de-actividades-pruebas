$(document).ready(function(){    
  $.post(
    "Controllers/AjaxController.php?funcion=verActividad",            
    {
        id : idActividad                
    },
    function(data,status){      


      var obj = $.parseJSON(data);

      $('#nombre').val(obj.nombre);
      $('#objetivo').text(obj.objetivo);
      $('#tipo').val(obj.tipo);
      $('#single_cal1').val(UnDateFormat(obj.fecha));      
      $('#temporalidad').val(obj.temporalidad);


      if(obj.convocatoria == 'Masiva'){
        $('#masiva').prop('checked', true);
        $('#masiva').click();        
        $('#tab-especifica').css('display','none');
        $('#tab-masiva').css('display','block');      
        

      }else if(obj.convocatoria == 'Especifica'){
        $('#convocatoria').prop('checked', true);
        $('#convocatoria').click();        
        $('#tab-masiva').css('display','none');
        $('#tab-especifica').css('display','block');        
      }


      var parid;
      var count = 0;
      for (i in obj.sesiones ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var sesion = '<tr class="'+parid+' pointer sesion"> <td>'+(count+1)+'</td> <td>'+obj.sesiones[i].nombre+'</td><td>'+obj.sesiones[i].descripcion+'</td> <td>'+UnDateFormat( obj.sesiones[i].fechaHora.substring(0, 10))+'</td> <td>'+obj.sesiones[i].fechaHora.substring(10, 16)+'</td>  <td> <i class="fa fa-trash-o close-sesion"></i></td> </tr>';    
        $('#table-sesiones').children().next().append(sesion);        
        count++;
      }      


      
      $('#idResponsable').val(obj.responsable.id);
      $('#responsable').val(obj.responsable.nombre);



      if(obj.estado == 'Pendiente'){
        $('#pendiente').prop('checked', true);
      }else if(obj.estado == 'Realizada'){
        $('#realizada').prop('checked', true);
      }      


      if(obj.resultado != 'NULL'){
        $('#resultado').text();
      }else if(obj.resultado == 'Realizada'){
        $('#realizada').prop('checked', true);
      }            
      

      $('#responsable').text(obj.responsable.nombre +' - '+ obj.responsable.unidadAcademica.nombre);

      count = 0;
      for (i in obj.colaboradores ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
        var colaborador = '<tr class="'+parid+' pointer colab"> <td>'+(count+1)+'</td> <td>'+obj.colaboradores[i].nombre+'</td> <td>'+obj.colaboradores[i].tipo+'</td> <td>'+obj.colaboradores[i].unidadAcademica.nombre+'</td>  <td></i> <i class="fa fa-trash-o close-colab"></i></td> <input type="hidden" value="'+obj.colaboradores[i].id+'" class="idColab"> </tr>';
        $('#table-colab').children().next().append(colaborador);
        count++;
      }      
      
      $('#convocatoria').text(obj.convocatoria);
      $('#estado').text(obj.estado);
      var resultado;

      $('#resultado').text(resultado);



      count = 0;      
      for (i in obj.beneficiarios ){
        if((count % 2) == 0){
          parid = 'odd';
        }else{
          parid = 'even';
        }
          var beneficiario = '<tr class="'+parid+' pointer benef"> <td>'+(count+1)+'</td> <td>'+obj.beneficiarios[i].nombre+'</td> <td>'+obj.beneficiarios[i].tipoIdentificacion+'</td> <td>'+obj.beneficiarios[i].identificacion+'</td> <td><i class="fa fa-trash-o close-benef"></i></td> </tr>';
      $('#table-benef').children().next().append(beneficiario);
        count++;
      }      
      
    
    }
  );

  $('#editar').click(function(){    
    if( tipoConvocatoria() == 'Especifica'){    
      if(tableEmpty('.benef', '#table-benef')){         
        editarActividad();  
      }
    }else if(  tipoConvocatoria() == 'Masiva'){          
      editarActividad();
    } 
  });


  function tipoConvocatoria(){
    var convocatoria = $("input[name='convocatoria']:checked").val();
    if(convocatoria == 'Especifica'){
      $('#tab-especifica').css('display','block');
      $('#tab-masiva').css('display','none');      
    }
    if(convocatoria == 'Masiva'){
      $('#tab-masiva').css('display','block');
      $('#tab-especifica').css('display','none');
    }    
    return convocatoria;
  }

  function tableEmpty(element, table){
    $('.alert-dismissible').remove();    
    var count = 0;
    $(element).each(function(){      
      count++;
    });
    if(count == 0){      
      $(table).after('<div class="alert alert-danger alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button> Debe agregar al menos un elemento para la actividad </div>');
      return false;
    }else{
      return true;      
    }      
  }


  function editarActividad(){

    var nombre = $('#nombre').val();
    var objetivo = $('#objetivo').val();
    var tipo = $('#tipo').val();
    var convocatoria = $("input[name='convocatoria']:checked").val();
    var listSesiones = sesiones();
    var listColab = colaboradores();            
    var responsable = $('#idResponsable').val();
    var estado = $("input[name='estado']:checked").val();
    var resultado = $('#resultado').val();      
    var fecha = dateFormat($('#single_cal1').val());      
    var temporalidad = $('#temporalidad').val();      
    var listBenef;
    var volumen;
    if( tipoConvocatoria() == 'Especifica'){
      listBenef = beneficiarios();  
      volumen = listBenef.length;
    }else if(  tipoConvocatoria() == 'Masiva'){          
      volumen = $('#volumen').val();
    }   
    var obj = '{ "id":"'+idActividad+'", "nombre":"'+nombre+'", "objetivo":"'+objetivo+'", "fecha":"'+fecha+'", "temporalidad":"'+temporalidad+'", "tipo":"'+tipo+'", "convocatoria":"'+convocatoria+'", "idResponsable":"'+responsable+'", "estado":"'+estado+'", "volumen":"'+volumen+'", "resultado":"'+resultado+'", "creador":"'+idUsuario+'", "sesiones": [';        
    for (var i = 0; i < listSesiones.length; i++) {
      if((i+1) == listSesiones.length){
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "fechaHora":"'+listSesiones[i].fechaHora+'"}';  
      }else{          
        obj += '{ "numero":"'+listSesiones[i].numero+'", "nombre":"'+listSesiones[i].nombre+'", "descripcion":"'+listSesiones[i].descripcion+'", "fechaHora":"'+listSesiones[i].fechaHora+'"},';  
      }      
    }

    if( tipoConvocatoria() == 'Especifica'){
      obj += '], "beneficiarios": [';
      for (var i = 0; i < listBenef.length; i++) {
        if((i+1) == listBenef.length){
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'"}'  
        }else{
          obj += '{ "numero":"'+listBenef[i].numero+'", "nombre":"'+listBenef[i].nombre+'", "tipoIdentificacion":"'+listBenef[i].tipoIdentificacion+'", "identificacion":"'+listBenef[i].identificacion+'"},'  
        }      
      }
    }

   
    obj += '], "idColaboradores": [';
    for (var i = 0; i < listColab.length; i++) {
      if((i+1) == listColab.length){
        obj += '"'+listColab[i].id+'"';  
      }else{
        obj += '"'+listColab[i].id+'",';
      }      
    }
    obj += ']}';  
    
    $.post(
      "Controllers/AjaxController.php?funcion=editarActividad",            
      {
          json : obj                
      },
      function(data,status){              
        if(data != 0){
          swal({                
            title: "Actividad Editada",
            text: "La actividad ha sido editada exitosamente, los detalles se mostraran a continuacion ",
            type: "success"
          }, function(){
            window.location.href = 'actividad.php?idActividad='+data;  
          }); 
          
        }else{
          swal({                
            title: "Error",
            text: "Ha ocurrido un error al editar la actividad",
            type: "error"
          }); 
        }
      }
    );  
  }

  function sesiones(){    
    var list = new Array();
    var sesion;
    $('.sesion').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "descripcion":$(this).children().eq(2).text(), 
          "fechaHora": dateTimeFormat( $(this).children().eq(3).text() ,  $(this).children().eq(4).text() ),          
        }        
        list.push(sesion);                    
    }); 
    return list;
  }

  function colaboradores(){    
    var list = new Array();
    var colab;
    $('.idColab').each(function() {      
        colab = { 
          "id":$(this).val()                  
        }
        list.push(colab);                    
    }); 
    return list;
  }

  function beneficiarios(){    
    var list = new Array();
    var benef;
    $('.benef').each(function() {      
        sesion = { 
          "numero":$(this).children().eq(0).text(), 
          "nombre":$(this).children().eq(1).text(), 
          "tipoIdentificacion":$(this).children().eq(2).text(),
          "identificacion":$(this).children().eq(3).text()
        }
        list.push(sesion);                    
    }); 
    return list;
  }


  function dateTimeFormat(date, time){        
    var age = date.substring(6, 10);
    var month = date.substring(0, 2);   
    var day =  date.substring(3, 5);
     
    var hour = time.substring(0, 3);
    var minute = time.substring(4, 6);        
    var dateTime = age+'-'+month+'-'+day+' '+hour+':'+minute;     
    return dateTime;     
  }

  function dateFormat(date){
     var age = date.substring(6, 10);
     var month = date.substring(0, 2);   
     var day =  date.substring(3, 5);

     var dateFormat = age+'-'+month+'-'+day;
    return dateFormat;     
  }  
  

  function UnDateFormat(date){        
    var age = date.substring(0, 4);
    var month = date.substring(5, 7);   
    var day =  date.substring(8, 10);

    var dateFormat = month+'/'+day+'/'+age;
    return dateFormat;     
  }   


});