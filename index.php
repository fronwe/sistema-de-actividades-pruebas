<!DOCTYPE html>
<html>


<head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vidas moviles |  Actividades</title>

    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="styles/css/animate.css">
    <link rel="stylesheet" href="styles/css/style.css">
    <link rel="stylesheet" href="styles/css/ld.css">


</head>
    <body style="background-image:url('styles/images/charity.png'); background-repeat: no-repeat ;background-attachment:fixed; background-position:center;">
    <div class="row">
        <div class="col-md-4">

        </div>  
        <div class="col-md-4">
            <div style="height: 150px" class="text-center">                
            </div>
            <div  class="panel-registro" style=""> 
            <h1 style="color: #4D4C6E; margin-top: 0px" class="logo-name text-center">VD</h1>                
                <h3 class="text-center">Bienvenido a Vidas Mobiles</h3>
                <p   style="color: #333">La plataforma para programar sus actividades </p>
                <p>Inicie sesión.</p>
                <form class="m-t" role="form" >
                    <div class="form-group">
                        <input id="email" type="email" class="form-control" placeholder="Correo" required="">
                        <p id="obligatorio" style="display: none" class="text-danger">Este campo es obligatorio</p>
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control" placeholder="Contraseña" required="">
                        <p style="display: none" class="text-danger">Este campo es obligatorio</p>
                    </div>

                    <div id="alerta" style="display: none" class="alert alert-danger">
                        <a id="close-alert" class="close">x</a>
                        <strong>Error de autenticacion</strong> El email o la contrasea no son correctos
                    </div>



                    <div id="registro" type="submit" class="btn btn-primary block full-width m-b">Entrar</div>


                    <a href="#"><small>Olvido su contraseña?</small></a>
                    <p class="text-muted text-center"><small>No esta registrado?</small></p>
                    <a class="btn btn-sm btn-white btn-block" href="register.html">Crear cuenta</a>
                </form>
            </div>
        </div>  
        <div class="col-md-4">

        </div>  
    </div> 

    <!-- Mainly scripts -->
    <script src="styles/js/jquery-2.1.1.js"></script>
    <script src="styles/js/bootstrap.min.js"></script>
    <script src="styles/js/registro.js"></script>

    <script>
        $(document).ready(function(){
            $('#registro').click(function(){

                var valido = true;

                if( $('#email').val() == '' ){
                    valido = false;
                    $('#email').next().fadeIn();
                }

                if( $('#password').val() == '' ){
                    valido = false;
                    $('#password').next().fadeIn();
                }

                if(valido){
                    
                    var email = $('#email').val();
                    var password = $('#password').val();
                    
                    $.post(
                        "Controllers/AjaxController.php?funcion=login",            
                        {
                            usuario : email,
                            clave : password 
                        },
                        function(data,status){                            
                            if(data == 1){                                
                                window.location.href = 'home.php';
                            }else{
                                $('#alerta').fadeIn();
                            }
                        }
                    );
                }                
            });

            $('#email').change(function(){
                $(this).next().fadeOut();
            });

            $('#password').change(function(){
                $(this).next().fadeOut();
            });

            $('#close-alert').click(function(){
               $(this).parent().fadeOut(); 
            });
        });
    </script>

</body>

</html>
