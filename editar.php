<!DOCTYPE html>
<html lang="en"></script>
  <head>
    <link rel="shortcut icon" href="favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></script>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1"></script>

    <title>Vidas moviles |  Actividades</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="styles/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/vendors/font-awesome/css/font-awesome.min.css">
    <!-- NProgress -->
    <link rel="stylesheet" href="styles/vendors/nprogress/nprogress.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="styles/vendors/iCheck/skins/flat/green.css">
	
    <!-- bootstrap-progressbar -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="styles/vendors/jqvmap/dist/jqvmap.min.css">
    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css">    
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="styles/build/css/custom.min.css"></script>
    <!-- bootstrap-daterangepicker -->
    <link href="styles/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- sweetalert -->
    <link href="styles/vendors/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="styles/vendors/clockpicker/clockpicker.css" rel="stylesheet">
    <link rel="stylesheet" href="styles/css/ld.css">

  </head>
  <?php 
    session_start();
    $colaborador;
    $session = '';
    $navAdmin = '';
    $idColaborador = '';
    $idActividad = '';
    
    if(isset($_SESSION['colaborador'])){
      $colaborador = json_decode($_SESSION['colaborador'],true);            
      $idColaborador = "var idUsuario = ".$colaborador['id'].";";

      if(isset($_GET['idActividad'])){
        $idActividad = 'var idActividad = '.$_GET['idActividad'];
      }else{
        $session = "window.location.href = 'home.php'";                    
      }
      if($colaborador['permiso'] == 'administrador'){
        $navAdmin = '<li> <a href="colaboradores.php"> <i class="fa fa-hand-paper-o"></i> Colaboradores</a> </li>';
      }

    }else{
      $session = "window.location.href = 'index.php'";                  
    }
  ?>
  <script type="text/javascript">
    <?php echo $session; ?>  
    <?php echo $idActividad; ?>  
    <?php echo $idColaborador; ?>  
  </script>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              
              <h1 class="text-center" style="font-size: 30px; font-weight: 20px; color :#FFF; opacity:0.8"><b>VD</b></h1>
            </div>

            <div class="clearfix"></div>
  
            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div style="  padding-top: 25px; padding-right: 10px; padding-bottom: 10px; padding-left: 30px" class="profile_pic">
                <h1 style="color: #fff; opacity: 0.8"><i class="fa fa-user"></i></h1>
              </div>
              <div class="profile_info">
                <span>Bienvenido</span>
                <h2><?php echo $colaborador['nombre'] ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->

            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">                
                <ul class="nav side-menu">
                  <li><a> <i class="fa fa-home"></i> Actividades <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="crear.php">Crear</a></li>
                      <li><a href="home.php">Actividades</a></li>                                    
                    </ul>
                  </li> 
                  <?php echo $navAdmin; ?>    
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          
          </div>
        </div>


        <!-- top navigation -->

        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"></i>
                    <?php echo $colaborador['nombre'] ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">                  
                    <li><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Cerrar Session</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12">
            <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-flag"></i> Actividades <small>Editar</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>      
                  <div class="x_content" style="display: block;">                  
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li  role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos Basicos</a>
                        </li>
                        <li style="display: none" role="presentation" id="tab2"><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Responsable</a>
                        </li>
                        <li style="display: none" role="presentation" id="tab3"><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Beneficiarios</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">                         
                          <div class="row">
                              <div class="col-md-4">
                                <p class="lead">Nombre actividad</p>                            
                                <input type="text" id="nombre" class="form-control validacion-tab1" placeholder="Nombre actividad">
                              </div>
                              <div class="col-md-4">
                                <p class="lead">Objetivo</p>
                                <textarea id="objetivo" class="form-control validacion-tab1" rows="2" placeholder="Objetivo de la actividad"></textarea>                                  
                              </div>
                              <div class="col-md-4">
                                <p class="lead">Tipo actividad</p>
                                <select id="tipo" class="form-control">
                                  <option>Capacitacion</option>
                                  <option>Taller</option>
                                  <option>Visita domiciliaria</option>
                                </select>                                   
                              </div>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-4">
                                <p class="lead">Fecha</p>
                               <!-- <input type="date" class="form-control" name="fecha" id="fecha">  -->


                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="daterangepicker dropdown-menu ltr single opensright show-calendar picker_4 xdisplay"><div class="calendar left single" style="display: block;"><div class="daterangepicker_input"><input class="input-mini form-control active" type="text" name="daterangepicker_start" value="" style="display: none;"><i class="fa fa-calendar glyphicon glyphicon-calendar" style="display: none;"></i><div class="calendar-time" style="display: none;"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"><table class="table-condensed"><thead><tr><th class="prev available"><i class="fa fa-chevron-left glyphicon glyphicon-chevron-left"></i></th><th colspan="5" class="month">Oct 2016</th><th class="next available"><i class="fa fa-chevron-right glyphicon glyphicon-chevron-right"></i></th></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><tr><td class="weekend off available" data-title="r0c0">25</td><td class="off available" data-title="r0c1">26</td><td class="off available" data-title="r0c2">27</td><td class="off available" data-title="r0c3">28</td><td class="off available" data-title="r0c4">29</td><td class="off available" data-title="r0c5">30</td><td class="weekend available" data-title="r0c6">1</td></tr><tr><td class="weekend available" data-title="r1c0">2</td><td class="available" data-title="r1c1">3</td><td class="available" data-title="r1c2">4</td><td class="available" data-title="r1c3">5</td><td class="available" data-title="r1c4">6</td><td class="available" data-title="r1c5">7</td><td class="weekend available" data-title="r1c6">8</td></tr><tr><td class="weekend available" data-title="r2c0">9</td><td class="available" data-title="r2c1">10</td><td class="available" data-title="r2c2">11</td><td class="available" data-title="r2c3">12</td><td class="available" data-title="r2c4">13</td><td class="available" data-title="r2c5">14</td><td class="weekend available" data-title="r2c6">15</td></tr><tr><td class="weekend available" data-title="r3c0">16</td><td class="available" data-title="r3c1">17</td><td class="today active start-date active end-date available" data-title="r3c2">18</td><td class="available" data-title="r3c3">19</td><td class="available" data-title="r3c4">20</td><td class="available" data-title="r3c5">21</td><td class="weekend available" data-title="r3c6">22</td></tr><tr><td class="weekend available" data-title="r4c0">23</td><td class="available" data-title="r4c1">24</td><td class="available" data-title="r4c2">25</td><td class="available" data-title="r4c3">26</td><td class="available" data-title="r4c4">27</td><td class="available" data-title="r4c5">28</td><td class="weekend available" data-title="r4c6">29</td></tr><tr><td class="weekend available" data-title="r5c0">30</td><td class="available" data-title="r5c1">31</td><td class="off available" data-title="r5c2">1</td><td class="off available" data-title="r5c3">2</td><td class="off available" data-title="r5c4">3</td><td class="off available" data-title="r5c5">4</td><td class="weekend off available" data-title="r5c6">5</td></tr></tbody></table></div></div><div class="calendar right" style="display: none;"><div class="daterangepicker_input"><input class="input-mini form-control" type="text" name="daterangepicker_end" value="" style="display: none;"><i class="fa fa-calendar glyphicon glyphicon-calendar" style="display: none;"></i><div class="calendar-time" style="display: none;"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"><table class="table-condensed"><thead><tr><th></th><th colspan="5" class="month">Nov 2016</th><th class="next available"><i class="fa fa-chevron-right glyphicon glyphicon-chevron-right"></i></th></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><tr><td class="weekend off available" data-title="r0c0">30</td><td class="off available" data-title="r0c1">31</td><td class="available" data-title="r0c2">1</td><td class="available" data-title="r0c3">2</td><td class="available" data-title="r0c4">3</td><td class="available" data-title="r0c5">4</td><td class="weekend available" data-title="r0c6">5</td></tr><tr><td class="weekend available" data-title="r1c0">6</td><td class="available" data-title="r1c1">7</td><td class="available" data-title="r1c2">8</td><td class="available" data-title="r1c3">9</td><td class="available" data-title="r1c4">10</td><td class="available" data-title="r1c5">11</td><td class="weekend available" data-title="r1c6">12</td></tr><tr><td class="weekend available" data-title="r2c0">13</td><td class="available" data-title="r2c1">14</td><td class="available" data-title="r2c2">15</td><td class="available" data-title="r2c3">16</td><td class="available" data-title="r2c4">17</td><td class="available" data-title="r2c5">18</td><td class="weekend available" data-title="r2c6">19</td></tr><tr><td class="weekend available" data-title="r3c0">20</td><td class="available" data-title="r3c1">21</td><td class="available" data-title="r3c2">22</td><td class="available" data-title="r3c3">23</td><td class="available" data-title="r3c4">24</td><td class="available" data-title="r3c5">25</td><td class="weekend available" data-title="r3c6">26</td></tr><tr><td class="weekend available" data-title="r4c0">27</td><td class="available" data-title="r4c1">28</td><td class="available" data-title="r4c2">29</td><td class="available" data-title="r4c3">30</td><td class="off available" data-title="r4c4">1</td><td class="off available" data-title="r4c5">2</td><td class="weekend off available" data-title="r4c6">3</td></tr><tr><td class="weekend off available" data-title="r5c0">4</td><td class="off available" data-title="r5c1">5</td><td class="off available" data-title="r5c2">6</td><td class="off available" data-title="r5c3">7</td><td class="off available" data-title="r5c4">8</td><td class="off available" data-title="r5c5">9</td><td class="weekend off available" data-title="r5c6">10</td></tr></tbody></table></div></div><div class="ranges" style="display: none;"><div class="range_inputs"><button class="applyBtn btn btn-sm btn-success" type="button">Apply</button> <button class="cancelBtn btn btn-sm btn-default" type="button">Cancel</button></div></div></div>                                  
                                  <fieldset>
                                    <div class="control-group">
                                      <div class="controls">
                                        <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="Fecha activdad" aria-describedby="inputSuccess2Status4">
                                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                          <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                        </div>
                                      </div>
                                    </div>
                                  </fieldset>
                                </div>                                
                            </div>
                            <div class="col-md-4">
                              <p class="lead">Temporalidad</p>
                              <div class="input-group clockpicker" data-autoclose="true">
                                <input id="temporalidad" type="text" class="form-control" value="12:00" >
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>      
                              </div>                          
                            </div>
                            <div class="col-md-4 ">
                                <p class="lead">Convocatoria</p>
                                <div class="convoca">
                                  <p><input type="radio" name="convocatoria" id="convocatoria" value="Especifica"/>  Especifica</p>
                                  <p><input type="radio" name="convocatoria" id="masiva" value="Masiva" /> Masiva</p>    
                                </div>                                  
                            </div>
                          </div>                       
                           
                          <hr>
                          <div class="row">
                            <div class="col-md-4">
                              <p class="lead">Agregar Sesiones</p>                        
                              <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="nombre">Nombre sesion </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="nombre-sesion" class="form-control" >
                                  </div>
                                </div>     

                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Descripcion </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" col="3" id="descripcion-sesion"></textarea>
                                  </div>
                                </div>     

                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha">Fecha</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="daterangepicker dropdown-menu ltr single opensright show-calendar picker_4 xdisplay"><div class="calendar left single" style="display: block;"><div class="daterangepicker_input"><input class="input-mini form-control active" type="text" name="daterangepicker_start" value="" style="display: none;"><i class="fa fa-calendar glyphicon glyphicon-calendar" style="display: none;"></i><div class="calendar-time" style="display: none;"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"><table class="table-condensed"><thead><tr><th class="prev available"><i class="fa fa-chevron-left glyphicon glyphicon-chevron-left"></i></th><th colspan="5" class="month">Oct 2016</th><th class="next available"><i class="fa fa-chevron-right glyphicon glyphicon-chevron-right"></i></th></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><tr><td class="weekend off available" data-title="r0c0">25</td><td class="off available" data-title="r0c1">26</td><td class="off available" data-title="r0c2">27</td><td class="off available" data-title="r0c3">28</td><td class="off available" data-title="r0c4">29</td><td class="off available" data-title="r0c5">30</td><td class="weekend available" data-title="r0c6">1</td></tr><tr><td class="weekend available" data-title="r1c0">2</td><td class="available" data-title="r1c1">3</td><td class="available" data-title="r1c2">4</td><td class="available" data-title="r1c3">5</td><td class="available" data-title="r1c4">6</td><td class="available" data-title="r1c5">7</td><td class="weekend available" data-title="r1c6">8</td></tr><tr><td class="weekend available" data-title="r2c0">9</td><td class="available" data-title="r2c1">10</td><td class="available" data-title="r2c2">11</td><td class="available" data-title="r2c3">12</td><td class="available" data-title="r2c4">13</td><td class="available" data-title="r2c5">14</td><td class="weekend available" data-title="r2c6">15</td></tr><tr><td class="weekend available" data-title="r3c0">16</td><td class="available" data-title="r3c1">17</td><td class="today active start-date active end-date available" data-title="r3c2">18</td><td class="available" data-title="r3c3">19</td><td class="available" data-title="r3c4">20</td><td class="available" data-title="r3c5">21</td><td class="weekend available" data-title="r3c6">22</td></tr><tr><td class="weekend available" data-title="r4c0">23</td><td class="available" data-title="r4c1">24</td><td class="available" data-title="r4c2">25</td><td class="available" data-title="r4c3">26</td><td class="available" data-title="r4c4">27</td><td class="available" data-title="r4c5">28</td><td class="weekend available" data-title="r4c6">29</td></tr><tr><td class="weekend available" data-title="r5c0">30</td><td class="available" data-title="r5c1">31</td><td class="off available" data-title="r5c2">1</td><td class="off available" data-title="r5c3">2</td><td class="off available" data-title="r5c4">3</td><td class="off available" data-title="r5c5">4</td><td class="weekend off available" data-title="r5c6">5</td></tr></tbody></table></div></div><div class="calendar right" style="display: none;"><div class="daterangepicker_input"><input class="input-mini form-control" type="text" name="daterangepicker_end" value="" style="display: none;"><i class="fa fa-calendar glyphicon glyphicon-calendar" style="display: none;"></i><div class="calendar-time" style="display: none;"><div></div><i class="fa fa-clock-o glyphicon glyphicon-time"></i></div></div><div class="calendar-table"><table class="table-condensed"><thead><tr><th></th><th colspan="5" class="month">Nov 2016</th><th class="next available"><i class="fa fa-chevron-right glyphicon glyphicon-chevron-right"></i></th></tr><tr><th>Su</th><th>Mo</th><th>Tu</th><th>We</th><th>Th</th><th>Fr</th><th>Sa</th></tr></thead><tbody><tr><td class="weekend off available" data-title="r0c0">30</td><td class="off available" data-title="r0c1">31</td><td class="available" data-title="r0c2">1</td><td class="available" data-title="r0c3">2</td><td class="available" data-title="r0c4">3</td><td class="available" data-title="r0c5">4</td><td class="weekend available" data-title="r0c6">5</td></tr><tr><td class="weekend available" data-title="r1c0">6</td><td class="available" data-title="r1c1">7</td><td class="available" data-title="r1c2">8</td><td class="available" data-title="r1c3">9</td><td class="available" data-title="r1c4">10</td><td class="available" data-title="r1c5">11</td><td class="weekend available" data-title="r1c6">12</td></tr><tr><td class="weekend available" data-title="r2c0">13</td><td class="available" data-title="r2c1">14</td><td class="available" data-title="r2c2">15</td><td class="available" data-title="r2c3">16</td><td class="available" data-title="r2c4">17</td><td class="available" data-title="r2c5">18</td><td class="weekend available" data-title="r2c6">19</td></tr><tr><td class="weekend available" data-title="r3c0">20</td><td class="available" data-title="r3c1">21</td><td class="available" data-title="r3c2">22</td><td class="available" data-title="r3c3">23</td><td class="available" data-title="r3c4">24</td><td class="available" data-title="r3c5">25</td><td class="weekend available" data-title="r3c6">26</td></tr><tr><td class="weekend available" data-title="r4c0">27</td><td class="available" data-title="r4c1">28</td><td class="available" data-title="r4c2">29</td><td class="available" data-title="r4c3">30</td><td class="off available" data-title="r4c4">1</td><td class="off available" data-title="r4c5">2</td><td class="weekend off available" data-title="r4c6">3</td></tr><tr><td class="weekend off available" data-title="r5c0">4</td><td class="off available" data-title="r5c1">5</td><td class="off available" data-title="r5c2">6</td><td class="off available" data-title="r5c3">7</td><td class="off available" data-title="r5c4">8</td><td class="off available" data-title="r5c5">9</td><td class="weekend off available" data-title="r5c6">10</td></tr></tbody></table></div></div><div class="ranges" style="display: none;"><div class="range_inputs"><button class="applyBtn btn btn-sm btn-success" type="button">Apply</button> <button class="cancelBtn btn btn-sm btn-default" type="button">Cancel</button></div></div></div>                                  
                                    <fieldset>
                                      <div class="control-group">
                                        <div class="controls">
                                          <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="single_cal4" placeholder="Fecha sesion" aria-describedby="inputSuccess2Status4">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                          </div>
                                        </div>
                                      </div>
                                    </fieldset>

                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Hora </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group clockpicker" data-autoclose="true">
                                      <input id="hora-sesion" type="text" class="form-control" value="09:30" >
                                      <span class="input-group-addon">
                                          <span class="fa fa-clock-o"></span>
                                      </span>
                                    </div>                                   
                                  </div>
                                </div>                                                            
                                <div class="form-group">                                                
                                  <button id="add-sesion" type="submit" class="btn btn-success pull-right col-md-3-offset"><i  class="fa fa-plus"></i> Agregar </button>            
                                </div>                                
                              </div>                                                            
                            </div>            
                            <div class="col-md-8">
                              <p class="lead">Sesiones</p>
                              <div class="x_content">                  
                                <div class="table-responsive">
                                  <table id="table-sesiones" class="table table-sesione table-striped jambo_table bulk_action">
                                    <thead>
                                      <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Nombre </th>
                                        <th class="column-title">Descripcion </th>
                                        <th class="column-title">Fecha </th>                                        
                                        <th class="column-title">Hora </th>
                                        <th class="column-title">Acciones </th>
                                      </tr>
                                    </thead>
                                    <tbody class="list-sesiones">                                
                                    </tbody>
                                  </table>                                
                                </div> 
                              </div>
                            </div>                            
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-10">
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <button id="sig-tab1" class="form-control btn btn-success">Siguiente</button>  
                              </div>                              
                            </div>
                          </div>
                        </div>  
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <div class="row">
                            <div class="col-md-3">      

                              <p class="lead">Tipo</p>
                              <select id="tipo-responsable" class="form-control">

                              </select>                              
                            </div>
                            <div class="col-md-3">      
                              <p class="lead">Unidad Academica</p>
                              <select id="unidadAcademica" class="form-control">
  
                              </select>                              
                            </div>                            
                            <div class="col-md-3">
                              <p class="lead">Agregar Responsable</p>                              
                              <select id="responsables" class="form-control">

                              </select>
                              <br>
                              <button id="select-resp" class="btn-success btn pull-right"><i class="fa fa-check"></i> Seleccionar</button>
                            </div>
                            <div class="col-md-3">    
                              <p class="lead">Responsable</p>   
                              <blockquote class="message"> <i class="fa fa-user"></i> <span id="responsable"></span>
                              <input type="hidden" id="idResponsable">
                              </blockquote>
                            </div>
                          </div>
                          <hr>
                          <div class="row">

                            <div class="col-md-4">
                              <p class="lead">Indicar Colaboradores</p>                        
                              <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tipo </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="tipo-colaborador" class="form-control">
                                    </select>                                   
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unidad Academica</label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="unidadAcademicaCol" class="form-control">
                                    </select> 
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Colaboradores </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="colaborador" class="form-control">
                                    </select>                                   
                                  </div>
                                </div>

                              
                                <!--<div class="ln_solid"></div>-->
                                <div class="form-group">                                                                      
                                    <button id="add-colab" type="submit" class="btn btn-success pull-right col-md-3 offset"><i  class="fa fa-plus"></i> Agregar </button>                                  
                                </div>                                
                              </div>

                            </div>

                            <div class="col-md-8">
                                <p class="lead">Colaboradores</p>
                                <div class="table-responsive">
                                  <table id="table-colab" class="table table-striped jambo_table bulk_action">
                                    <thead>
                                      <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Nombre </th>
                                        <th class="column-title">Tipo </th>
                                        <th class="column-title">Unida Academica </th>
                                        <th class="column-title">Acciones </th>
                                      </tr>
                                    </thead>
                                    <tbody class="list-colab">
                                    </tbody>
                                  </table>
                                </div>     
                                <div class="col-md-1">                                                       
                                </div>               
                            </div>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <button id="atr-tabHome" style="background: #3F5367" class="form-control btn btn-primary">Atras</button>  
                              </div>                              
                            </div>                            
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <button id="sig-tab2" class="form-control btn btn-success">Siguiente</button>  
                              </div>                              
                            </div>
                          </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade " id="tab_content3" aria-labelledby="profile-tab">
                          <div class="row">
                            <div class="col-md-4">
                              <p class="lead">Estado de la actividad</p>
                              <p> 
                                <input type="radio" checked="true" class="flat" name="estado" id="pendiente" value="Pendiente" />
                                  Pendiente
                              </p>
                              <p> 
                                <input type="radio" class="flat" name="estado" id="realizada" value="Realizada" />  
                                Realizada
                              </p>    
                            </div>
                            <div class="col-md-4">
                              <p class="lead">Resultado</p>
                              <textarea class="form-control" id="resultado" rows="2" placeholder="Resultado de la actividad"></textarea>
                            </div>
                            <div class="col-md-4">
                              <div style="display: none" id="tab-masiva">
                                <p class="lead">Volumen</p>
                                <input class="input-number"  id="volumen" type="number" name="" min="1" max="1000">
                              </div>                              
                            </div>
                          </div>
                          <hr>
                          <div  style="display: none" id="tab-especifica">
                            <div class="row">

                              <div class="col-md-4">
                                <p class="lead">Agregar Beneficiarios</p>                        
                               
                                <div class="form-horizontal form-label-left">
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nombre </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" class="form-control" id="nombre-beneficiario"  name="nombre-beneficiario" placeholder="Nombre Beneficario">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo de documento</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select id="tipo-documento" class="form-control">
                                        <option>Cedula de Ciudadania</option>
                                        <option>Tarjeta de identidad</option>  
                                        <option>Pasaporte</option>
                                        <option>Cedula de extranjero</option>  
                                        <option>Registro Civil</option>  
                                      </select> 
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Documento </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" class="form-control" id="documento" name="documento" placeholder="Documento Beneficiario">                                                                
                                    </div>
                                  </div>
                                  <div class="form-group">                                                                  
                                    <button id="add-benef" type="submit" class="btn btn-success pull-right col-md-3-offset"><i  class="fa fa-plus"></i> Agregar </button>                                
                                  </div> 
                                </div>

                              </div>    

                              <div class="col-md-8">
                                <p class="lead">Beneficiarios</p>
                                <div class="x_content">                  
                                  <div class="table-responsive">
                                    <table id="table-benef" class="table table-striped jambo_table bulk_action">
                                      <thead>
                                        <tr class="headings">
                                          <th class="column-title"># </th>
                                          <th class="column-title">Nombre </th>
                                          <th class="column-title">Tipo Identificacion </th>
                                          <th class="column-title">Identificacion </th>
                                          <th class="column-title">Acciones </th>                                          
                                        </tr>
                                      </thead>
                                      <tbody class="list-benef">                                                                                                             
                                      </tbody>
                                    </table>
                                  </div> 
                                </div>
                              </div>   
                            </div>
                            <hr>
                          </div>
                          <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <button id="atr-profileTab" style="background: #3F5367" class="form-control btn btn-success">Atras</button>  
                              </div>                              
                            </div>
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-2">
                              <div class="form-group">
                                <button id="editar" class="form-control btn btn-success">Editar</button>  
                                
                              </div>                              
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->        
        
          <!-- footer content -->
          <footer>
            <div class="pull-right"></script>
              Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com"></script>Colorlib</a>
            </div>
            <div class="clearfix"></script></div>
            <p class="p"></p>
          </footer>
          <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="styles/vendors/jquery/dist/jquery.min.js"></script>
    <!--<script src="styles/js/bootstrap.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="styles/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="styles/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="styles/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="styles/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="styles/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="styles/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="styles/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="styles/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="styles/vendors/Flot/jquery.flot.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.time.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="styles/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="styles/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="styles/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="styles/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="styles/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="styles/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="styles/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="styles/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="styles/build/js/custom.min.js"></script>
        <!-- bootstrap-daterangepicker -->
    <script src="styles/vendors/moment/min/moment.min.js"></script>
    <script src="styles/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- sweetalert -->
    <script src="styles/vendors/sweetalert/sweetalert.min.js"></script>

    <script src="styles/vendors/clockpicker/clockpicker.js"></script> 

    
    <script src="styles/js/actividad.js"></script>
    <script src="styles/js/editar.js"></script>
    
    <script>
        $(document).ready(function(){

            $('.clockpicker').clockpicker();
            $('#logout').click(function(){
              $.get(
                  "Controllers/AjaxController.php?funcion=logout",
                  function(data,status){                                                                              
                    window.location.href = 'index.php';                 
                  }
              );
                             
            });

        });
    </script>
  </body>
</html>
