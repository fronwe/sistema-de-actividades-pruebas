<?php

//require_once 'C:/xampp/htdocs/PSU/Models/Colaborador.php';
require_once '../Models/Colaborador.php';

class ColaboradorController{
	
	public function colaborador($id, $mysqli){
		$obj = new Colaborador($id, $mysqli);
		return $obj;
	}

	public function colaboradores($mysqli){
		$lista = array();
		$cont = 0;
		$query = 'SELECT id FROM colaboradores WHERE 1 ';		
		$peticion = $mysqli->query($query);
		if($peticion->num_rows > 0){
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				
				$obj = new Colaborador($fila['id'], $mysqli);
				$lista[$cont] = $obj;
				$cont++;
			}
			return $lista;
		}else{
			return NULL;
		}
	}

	public function asignarColaboradores($idActividad, $idColaborador, $mysqli){				
		$query = "INSERT INTO `actividades_colaboradores` (`id_actividades`, `id_colaboradores`) VALUES ('".$idActividad."', '".$idColaborador."');";		
		$peticion = $mysqli->query($query);	
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}		

	public function crearColaborador($colaborador, $mysqli){				
		$query = "INSERT INTO `colaboradores` (`id`, `nombre`, `email`, `tipo_identificacion`, `identificacion`, `tipo`, `clave`, `permiso`, `id_unidad_academica`) VALUES (NULL, '".$colaborador->nombre."', '".$colaborador->email."', '".$colaborador->tipoIdentificacion."', '".$colaborador->identificacion."', '".$colaborador->tipo."', '".$colaborador->clave."', '".$colaborador->usuario."', '".$colaborador->unidad."');"; 
			
		$peticion = $mysqli->query($query);	
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}		

	public function tiposColaborador($mysqli){
		$query = "SELECT tipo FROM `colaboradores` WHERE 1 group by tipo";		
		$peticion = $mysqli->query($query);
		$tipos = array();
		if($peticion && $peticion->num_rows > 0){
			
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				$tipos[$num] = $fila['tipo'];			
			}
			return $tipos;;
		}else{
			return 0;
		}				
	}

	public function areasAcademicasTipo($tipo, $mysqli){
		$query = "SELECT unidadesacademicas.nombre As area, unidadesacademicas.id AS id FROM colaboradores, unidadesacademicas WHERE colaboradores.tipo = '".$tipo."' AND colaboradores.id_unidad_academica = unidadesacademicas.id GROUP BY unidadesacademicas.nombre";		
		$peticion = $mysqli->query($query);
		$areas = array();
		if($peticion && $peticion->num_rows > 0){			
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				$areas[$num]['id'] = $fila['id'];
				$areas[$num]['area'] = $fila['area'];
			}
			return $areas;
		}else{
			return 0;
		}				
	}	

	public function colaboradoresArea($tipo, $idArea, $mysqli){
		$query = "SELECT colaboradores.nombre, colaboradores.identificacion, colaboradores.id FROM colaboradores, unidadesacademicas WHERE unidadesacademicas.id = colaboradores.id_unidad_academica AND unidadesacademicas.id = ".$idArea." AND colaboradores.tipo = '".$tipo."'";		
		$peticion = $mysqli->query($query);
		$colaboradores = array();
		if($peticion && $peticion->num_rows > 0){			
			for($num = $peticion->num_rows - 1; $num>=0;$num--){
				$fila = $peticion->fetch_assoc();
				$colaboradores[$num]['id'] = $fila['id'];
				$colaboradores[$num]['nombre'] = $fila['nombre'];
				$colaboradores[$num]['identificacion'] = $fila['identificacion'];				
			}
			return $colaboradores;
		}else{
			return 0;
		}				
	}


	public function eliminarColaborador($idColaborador, $mysqli){
		$query = 'DELETE FROM `colaboradores` WHERE id = '.$idColaborador;		
		$peticion = $mysqli->query($query);
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}


	public function editarColaborador($colaborador, $mysqli){
		$query = "UPDATE `colaboradores` SET `nombre`= '".$colaborador->nombre."', `email`= '".$colaborador->email."', `tipo_identificacion`= '".$colaborador->tipoIdentificacion."', `identificacion`= '".$colaborador->identificacion."', `tipo`= '".$colaborador->tipo."', `clave`= '".$colaborador->clave."', `permiso`= '".$colaborador->usuario."', `id_unidad_academica`= '".$colaborador->unidad."'WHERE id = ".$colaborador->id;		
		$peticion = $mysqli->query($query);
		if($peticion){
			return 1;
		}else{
			return 0;
		}
	}





}

?>
